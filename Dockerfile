FROM centos:7

#FROM anstrelnikov/docker-centos-java-mvn

MAINTAINER "HarunKumar Chanduluru" <harunkumar.c@nirman.io>

ENV HOST=0.0.0.0 \
    PORT=9966

RUN yum -y install java-1.8.0-openjdk.x86_64 sudo maven

RUN useradd petclinic

WORKDIR /home/petclinic

RUN echo "petclinic ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers

ENV JAVA_HOME="/usr/lib/jvm/jre-1.8.0-openjdk"

COPY . /home/petclinic

RUN cd /home/petclinic && \
  echo "#!/bin/bash" > start.sh && \
  echo "cd /home/petclinic && mvn tomcat7:run-war" >> start.sh

RUN chmod +x /home/petclinic/start.sh

EXPOSE $PORT

CMD ["/bin/bash", "/home/petclinic/start.sh"]